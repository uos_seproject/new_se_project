package P_Path;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;


import P_External.MapManager;


public class PathManager
{
	private ArrayList<Point> path; // 실제 경로
	private MapManager addonMap;
	private Point dest; // 종료 지점
	private Point start; // 시작 지점
	private ArrayList<Node> closeSet; // 최소 비용으로 선택된 노드 집합
	private ArrayList<Node> openSet; // 갈 수있는 노드 집합, 최소 비용으로 선택된 노드들은 제외됨
	private int pathIndex; // 실제 경로 인덱스
	
	public PathManager(MapManager addonMap)
	{
		this.addonMap = addonMap;
		this.path = new ArrayList<Point>();
		this.closeSet = new ArrayList<Node>();
		this.openSet = new ArrayList<Node>();
	}

	/*
	 * @see P_Path.PathManager#setDest(Point)
	 * 목적지를 설정한다
	 */
	public void setDest(Point dest)
	{
		this.dest = dest;
	}
	
	/*
	 * @see P_Path.PathManager#setStart(Point)
	 * 시작지점을 설정한다
	 */
	public void setStart(Point start)
	{
		this.start = start;
	}
	
	/* 
	 * @see P_Path.PathManager#getMinCostIndex(ArrayList<Node>)
	 * list에서 최소 cost를 가지는 node의 인덱스를 찾는다
	 * argument:
	 * 		list : ArrayList<Node>
	 * return:
	 * 		리스트에서 최소 cost를 가지는 node의 인덱스
	 */
	private int getMinCostIndex(ArrayList<Node> list)
	{
		int minCostIndex = 0;
		Node minCostNode = list.get(minCostIndex);
		int size = list.size();
		Node curNode;
		
		//using linear search
		for(int i=1; i<size; i++)
		{
			curNode = list.get(i);
			if(curNode.getCost() < minCostNode.getCost())
			{
				minCostIndex = i;
				minCostNode = curNode;
			}
		}
		return minCostIndex;
	}
	
	/*
	 * @see P_Path.PathManager#generatePath()
	 * A*알고리즘을 이용하여 경로를 생성한다
	 */
	public void generatePath()
	{
		// 이전에 있던 경로, closeSet, openSet을 모두 초기화
		this.path.clear();
		this.closeSet.clear();
		this.openSet.clear();
		int minCostIndex;
		Node pivot;
		
		// 시작지점을 openSet에 등록하고 while 루프에 들어간다.
		addInOpenSet((int)this.start.getX(), (int)this.start.getY(), null);
		pivot = openSet.get(0);
		
		/*
		 * 선택된 pivot이 dest와 좌표가 일치 할 때까지 while 루프를 반복한다
		 */
		while(((int)pivot.getX() == (int)this.dest.getX() &&
			   (int)pivot.getY() == (int)this.dest.getY())==false)
		{
			addAroundNodesInOpenSet(pivot); // pivot의 주변 노드들을 openSet에 등록한다
			minCostIndex = getMinCostIndex(openSet); // openSet에서 최소 비용 노드의 인덱스를 얻는다
			pivot = openSet.remove(minCostIndex); // pivot을 openSet의 최소비용 노드로 선택하고, openSet에서는 지운다
			this.closeSet.add(pivot); // pivot을 closeSet에 등록한다
		}
		
		
		/*
		 * 이 루프를 들어가기 직전에, pivot은 dest에 도착한 상태이다
		 * 따라서 pivot의 parent를 게속 부르면 시작지점까지 도착할 것이다.
		 * 또한 시작지점의 parent는 null로 설정되어있으므로 시작지점에 도착하면 루프가 종료된다
		 */
		while(pivot!=null)
		{
			this.path.add(pivot); // 실제 경로에 pivot을 넣어준다
			pivot = pivot.getParent(); // parent를 pivot에 넣어 루프를 반복한다
		}
		// 위 while루프가 종료된 후 path는 dest에서부터 start까지 경로를 가지고 있다.
		// 하지만 경로가 역방향으로 되어있으므로 경로를 뒤집어 실제 경로를 완성한다
		Collections.reverse(this.path);
	
		// 실제 경로의 첫 노드가 시작지점의 노드와 같다면 그 노드는 지운다.
		// getNextPath()는 언제나 다음 path의 다음 노드를 주는데,
		// 시작지점과 다음에 얻을 노드가 일치한다면 오류가 생기므로 지운다
		if((int)this.path.get(0).getX() == (int)this.start.getX() && 
		   (int)this.path.get(0).getY() == (int)this.start.getY())
		{
			this.path.remove(0); 
		}
		
		// path 인덱스를 0으로 설정하고 리턴한다.
		this.pathIndex = 0;
		return;
	}
	
	/*
	 * @see P_Path.PathManager#addAryoundNodeInSet(ArrayList<Node>, Node)
	 * node의 주변 노드들을 list에 추가한다
	 * argument:
	 * 		list: arraylist
	 * 		node: pivot node
	 */
	private void addAroundNodesInOpenSet(Node pivot)
	{
		int nodeX = (int)pivot.getX();
		int nodeY = (int)pivot.getY();
		addInOpenSet(nodeX, nodeY-1, pivot); // arg로 받은 노드의 북쪽 노드를 리스트에 추가
		addInOpenSet(nodeX+1, nodeY, pivot); // arg로 받은 노드의 동쪽 노드를 리스트에 추가
		addInOpenSet(nodeX, nodeY+1, pivot); // arg로 받은 노드의 남쪽 노드를 리스트에 추가
		addInOpenSet(nodeX-1, nodeY, pivot); // arg로 받은 노드의 서쪽 노드를 리스트에 추가
	}
	
	/*
	 * @see P_Path.PathManager#addInList(ArrayList<Node>, int, int, Node)
	 * list에 (x,y)에 해당하는 노드를 추가하고 그 노드의 부모를 parent로 설정한다
	 * argument:
	 * 		list:
	 * 		x,y: new Node (x,y)
	 * 		parent: the parent node of the new Node
	 */
	private void addInOpenSet(int x, int y, Node parent)
	{
		if(this.addonMap.isInMap(x, y) == false) return; // (x,y)가 맵의 밖이라면 무시하고 리턴한다
		if(this.addonMap.getHazard(x,y)) return; // (x,y)에 hazard가 존재한다면 무시하고 리턴한다
		
		// (x,y)가 이미 list에 존재하는 노드라면 무시하고 리턴한다
		// 선형 탐색을 이용하였다.
		int i, size = this.openSet.size();
		for(i=0; i<size; i++)
		{
			if((int)this.openSet.get(i).getX() == x && 
			   (int)this.openSet.get(i).getY() == y)
			{
				return;
			}
		}

		// (x,y)가 이미 closeSet에 등록되어있다면 무시하고 리턴한다
		size = this.closeSet.size();
		for(i=0; i<size; i++)
		{
			if((int)this.closeSet.get(i).getX() == x &&
			   (int)this.closeSet.get(i).getY() == y)
			{
				return;
			}
		}
		
		// (x,y)가 list에 존재하지않고, closeSet에 존재하지않고, 맵 내에있고, hazard가 없으면 list에 추가한다
		this.openSet.add(new Node(x, y, this.dest, parent));
	}
	
	/*
	 * @see P_Path.PathManager#getNextPath()
	 * return:
	 * 		다음 경로으 좌표
	 */
	public Point getNextPath(){
		if(path.size() == pathIndex)
		{ // 모든 경로를 탐색했을 경우
			throw new IndexOutOfBoundsException("모든 경로를 탐색하였습니다.");
		}
		return this.path.get(this.pathIndex++);
	}
}
