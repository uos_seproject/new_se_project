package P_Path;

import java.awt.Point;

@SuppressWarnings("serial")
public class Node extends Point
{
	private int cost; // 현재 Node의 위치에서 종료지점까지의 거리 abs(x-end.X) + abs(y-end.Y)로 계산된다
	private Node parent; // 이 노드의 부모 노드
	
	/* Constructor
	 * argument:
	 * 		(x,y) 이 노드의 좌표
	 * 		end: 종료 지점 ( cost 계산을 위함)
	 * 		parent: 부모 노드
	 */
	public Node(int x, int y, Point end, Node parent)
	{
		super(x,y);
		this.cost = (int) (Math.abs((int)end.getX()-(int)super.getX()) 
						   + Math.abs((int)end.getY()-(int)super.getY()));
		this.parent = parent;
	}

	/* Constructor
	 * argument:
	 * 		pos: 등록할 좌표를 가지고 있는 Point
	 * 		end: 종료 지점
	 * 		parent: 부모 노드
	 */
	public Node(Point pos, Point end, Node parent)
	{
		this((int)pos.getX(), (int)pos.getY(), end, parent);
	}
	
	/*
	 * @see P_Path.Node#getCost()
	 * return:
	 * 		cost
	 */
	public int getCost()
	{
		return this.cost;
	}
	
	/*
	 * @see P_Path.Node#getParent()
	 * return:
	 * 		부모 노드
	 */
	Node getParent()
	{
		return this.parent;
	}
	
	/*
	 * @see java.awt.Point#toString() 
	 */
	@Override
	public String toString()
	{
		return "(" + super.getX() + "," + super.getY() + "- "+this.cost + ")";
	}
}
