package Boundaries;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import P_ADD_ON.ADD_ON;
import P_ADD_ON.SIMManager;

@SuppressWarnings("serial")
public class MainForm extends JFrame
{
	private JPanel upperPanel = new JPanel();
	private JFileChooser jfc = new JFileChooser();
	private JButton buttonOpen = new JButton("Open");
	private JLabel fileLabel = new JLabel();
	private JButton buttonNext = new JButton("MoveNext");
	private JPanel gridPanel;
	private JPanel mainPanel;
	private GridLayout gridMap;
	private JPanel[][] gridPanels;
	
	public Image simImage = null;
	public Image hazardImage = null;
	public Image colorBlobImage = null;
	public Image unHazardImage = null;
	public Image unColorBlobImage = null;
	public Image destImage = null;
	
	private JLabel simLabel = null;
	
	private Point mapSize;
	private Point initCoord;
	private Point[] dests;
	private Point[] hazards;
	private ADD_ON addon;
	private SIMManager sm;
	
	public MainForm()
	{
		setSize(768,768);									// form의 크기
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		setTitle("Simulator");								// form의 title Simulator로 설정
		this.mainPanel = new JPanel();						// mainPanel 생성
		add(this.mainPanel);								// form에 생성한 mainPanel add
		this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.PAGE_AXIS));	// mainPanel Layout 설정
		this.mainPanel.add(this.upperPanel);			
		this.upperPanel.setLayout(new FlowLayout());
		this.upperPanel.setMaximumSize(new Dimension(300, 100));
		this.upperPanel.setMinimumSize(new Dimension(300, 100));
		this.upperPanel.add(this.fileLabel);
		this.upperPanel.add(this.buttonOpen);
		this.setResizable(false);

		// 이미지 불러오기 
		String path = (new File("").getAbsolutePath())+"\\res\\images\\";
		try
		{
			simImage =  Toolkit.getDefaultToolkit().getImage(path + "sim.jpg");
			hazardImage = Toolkit.getDefaultToolkit().getImage(path+ "hazard.jpg");
			colorBlobImage = Toolkit.getDefaultToolkit().getImage(path + "colorblob.jpg");
			unHazardImage = Toolkit.getDefaultToolkit().getImage(path + "unhazard.jpg");
			unColorBlobImage =Toolkit.getDefaultToolkit().getImage(path + "uncolorblob.jpg");
			destImage = Toolkit.getDefaultToolkit().getImage(path + "dest.jpg");
		}
		catch(NullPointerException e)
		{
			System.out.println(e.getMessage());
		}
		
		
		// file 여는 버튼을 눌렀을 때
		buttonOpen.addActionListener
		(
			new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e) 
				{
					if(jfc.showOpenDialog(upperPanel) == JFileChooser.APPROVE_OPTION)		// 파일 선택할수 있는 탐색창 띄움
					{
						if(parsing(jfc.getSelectedFile().toString())==false) return;	// 올바른 형식이 아니면 return함
						
						try
						{
							sm = new SIMManager(hazards, dests, initCoord, mapSize);		
							addon = new ADD_ON(hazards, dests, initCoord, mapSize, sm);		
						}
						catch(Exception ex)
						{
							JOptionPane.showMessageDialog(null, ex.getMessage());
							return;
						}
						
						// map 크기 변수 
						int rows = (int)mapSize.getX();	
						int cols = (int)mapSize.getY();
						
						// gridPanel에 gridMap 그림
						gridPanel = new JPanel();
						gridMap = new GridLayout(cols, rows);
						gridPanels = new JPanel[cols][rows];
						gridPanel.setLayout(gridMap);
						
						// gridPanel를 지도 크기에 맞게 지도 생성
						for(int i=0; i<cols; i++)
						{
							gridPanels[i] = new JPanel[rows];
							for(int j=0; j<rows; j++)
							{
								gridPanels[i][j] = new JPanel();
								gridPanel.add(gridPanels[i][j]);
								gridPanels[i][j].setBackground(Color.WHITE);
								gridPanels[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
								
							}
						}
						
						// 이미 open했는데 다시 open할 경우 이전에 그린 지도를 지우기 위해
						if(mainPanel.getComponentCount() == 2)
						{
							mainPanel.remove(mainPanel.getComponentCount()-1);
						}
						mainPanel.add(gridPanel);
						setVisible(true);
						
						// ----- 이미지 불러와 크기에 맞게 설정  start---
						int imageSizeX = gridPanels[0][0].getWidth()-10;
						int imageSizeY = gridPanels[0][0].getHeight()-12;
						simImage = simImage.getScaledInstance(imageSizeX, imageSizeY, 0);
						simLabel = new JLabel(new ImageIcon(simImage));	// sim은 하나이므로 Label 미리 하나 할당
						hazardImage = hazardImage.getScaledInstance(imageSizeX, imageSizeY, 0);
						unHazardImage = unHazardImage.getScaledInstance(imageSizeX, imageSizeY, 0);
						colorBlobImage = colorBlobImage.getScaledInstance(imageSizeX, imageSizeY, 0);
						unColorBlobImage = unColorBlobImage.getScaledInstance(imageSizeX, imageSizeY, 0);
						destImage = destImage.getScaledInstance(imageSizeX, imageSizeY, 0);
						// ----- 이미지 불러와 크기에 맞게 설정 end---
						
						display();
						upperPanel.setSize(getWidth(), buttonOpen.getHeight()+2);
					}
				}
			}
		);
		
		this.upperPanel.add(this.buttonNext);
		// MoveNext 버튼을 눌렀을 경우
		this.buttonNext.addActionListener
		(
			new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(mapSize != null)	// 초기화 되어있지 않으면 아무런 작업을 안하게 막아줌
					{
						moveNext();
					}
				}
			}
		);
		
		jfc.setMultiSelectionEnabled(false);
		setVisible(true);
	}

	/*
	 * text파일을  읽어와 "Map", "Start", "Spot", "Hazard"에대해 각 해당 변수에 값 넣어주는 함수
	 */
	public boolean parsing(final String path)
	{
		String s;
		String str;	
		char[] arr;
		int i,j;
		
		try 
		{
			BufferedReader in = new BufferedReader(new FileReader(path));
			while ((str = in.readLine()) != null) 
			{ // text를 끝날때까지 읽어옴
				s = str;
				arr = str.toCharArray(); // string을 array형태로 변환

				// 문자열에 문자가 ':'일때까지 i를 증가시켜 인덱스를 찾음
				for (i = 0; i < arr.length && arr[i] != ':'; i++);
				
				s = String.copyValueOf(arr, 0, i); // arr배열에 :전까지 string s에 복사
				
				Point[] posTemp = extractPoints(str, i); // extractPoints로 부터 반환되는 Point값을 받기위한 변수

				// s가 각 지정된 문자열 일때 각 항목에 관하여 저장
				if (s.equals("Map")) 
				{
					this.mapSize = new Point((int)posTemp[0].getX(), (int)posTemp[0].getY());
				} 
				else if (s.equals("Start")) 
				{
					this.initCoord = new Point((int)posTemp[0].getX(), (int)posTemp[0].getY());
				} 
				else if (s.equals("Spot")) 
				{
					this.dests = new Point[posTemp.length];
					for (j = 0; j < posTemp.length; j++)
					{
						dests[j] = new Point((int)posTemp[j].getX(), (int)posTemp[j].getY());
					}
				} 
				else if (s.equals("Hazard")) {
					this.hazards = new Point[posTemp.length];
					for (j = 0; j < posTemp.length; j++)
					{
						hazards[j] = new Point((int)posTemp[j].getX(), (int)posTemp[j].getY());
					}
				} 
				else 
				{
					JOptionPane.showMessageDialog(null, "파일 형식이 다릅니다.");
					in.close();
					return false;
				}
			}
			in.close();
		} 
		catch (IOException e)
		{
	        System.err.println(e.getMessage());
	    }
		return true;
	}

	/*
	 * parameter로 받아온 문자열 str에서 각 좌표를 추출하여 반환하는 함수
	 */
	public Point[] extractPoints(String str, int index)
	{	// 괄호안의 숫자를 추출하여 Point[]로 반환
		ArrayList<Point> pos = new ArrayList<Point>();
		Integer front;
		char[] arr;
		arr = str.toCharArray();
		int j,i;
		Stack<Integer> pStack = new Stack<Integer>();
				
		for(i=index;i<str.length();i++)
		{
			if(arr[i]=='(' && arr[i+1]!='(')	//stack에 '('의 위치를 저장
			{
				  pStack.push(i);
			}
			else if(arr[i]==')' && arr[i-1]!=')')
			{
				  front = (Integer)pStack.pop();	// front는 '('의 위치
				  for(j=(int)front; j< i;j++)
				  {
					  if(arr[j]==' ')
					  {		// 중간에 ' '을 기준으로  '(' 부터 ' '까지 Point X에 저장 / ' '에서 ')'까지 Point Y에 저장
						  pos.add(new Point(
								  (int)Integer.parseInt(str.substring((int)front+1,j)),
								  (int)Integer.parseInt(str.substring(j+1,i))));
					  }
				  }
			 }
		}
		
		// string을 Array형태로 변환
		Point[] ret = new Point[pos.size()];
		ret = pos.toArray(ret);
		return ret;	
	}
	
	/*
	 * 사용자에게 보여주는 UI를 띄우는 함수
	 */
	public void display()
	{
		Point[] allColorBlobs = sm.getAllColorBlobs();
		Point[] allHazards = sm.getAllHazards();
		Point[] discoveredHazards = addon.getDiscoverdHazards();
		Point[] discoveredColorBlobs = addon.getDiscoveredColorBlobs();
		int i, j;
		int x, y;
		
		int mapSizeX = (int)mapSize.getX();
		int mapSizeY = (int)mapSize.getY();
		// Map 초기화
		for(i=0; i<mapSizeY; i++)
		{
			for(j=0; j<mapSizeX; j++)
				gridPanels[i][j].setBackground(Color.WHITE);
		}
		// 발견하지 못한 color blobs 그리기
		for(i=0; i<allColorBlobs.length; i++)
		{
			x = (int)allColorBlobs[i].getX();
			y = (int)allColorBlobs[i].getY();
			gridPanels[y][x].setBackground(null);
			if(gridPanels[y][x].getComponentCount()==0)
			{
				gridPanels[y][x].add(new JLabel(new ImageIcon(unColorBlobImage)));
			}
		}
		
		// 발견한 color blobs 그리기
		for(i=0; i<discoveredColorBlobs.length; i++)
		{
			x = (int)discoveredColorBlobs[i].getX();
			y = (int)discoveredColorBlobs[i].getY();
			
			gridPanels[y][x].setBackground(null);
			
			// 이전에 gridPanel에 add되어 있는 발견하지 못한 colorBlob지우기
			while(gridPanels[y][x].getComponentCount()>0)
			{
				gridPanels[y][x].remove(gridPanels[y][x].getComponentCount()-1);
			}
			gridPanels[y][x].add(new JLabel(new ImageIcon(colorBlobImage)));

		}
		// 목적지 그리기
		for(i=0; i<this.dests.length; i++)
		{
			x = (int)dests[i].getX();
			y = (int)dests[i].getY();
			if(gridPanels[y][x].getComponentCount()==1)
			{
				gridPanels[y][x].remove(0);
			}
			if(gridPanels[y][x].getComponentCount()==0)
			{	
				gridPanels[y][x].add(new JLabel(new ImageIcon(destImage)));
			}			
		}
		
		// 발견되지 못한 hazard 그리기
		for(i=0; i<allHazards.length; i++)
		{
			x = (int)allHazards[i].getX();
			y = (int)allHazards[i].getY();
			while(gridPanels[y][x].getComponentCount()>0)
			{	
				gridPanels[y][x].remove(gridPanels[y][x].getComponentCount()-1);
			}
			gridPanels[y][x].setBackground(null);
			gridPanels[y][x].add(new JLabel(new ImageIcon(unHazardImage)));
		}
		
		// 발견한 hazard 그리기
		for(i=0; i<discoveredHazards.length; i++)
		{
			x = (int)discoveredHazards[i].getX();
			y = (int)discoveredHazards[i].getY();
			// 이전에 gridPanel에 add되어 있는 발견하지 못한 hazard지우기
			while(gridPanels[y][x].getComponentCount()>0)
			{	
				gridPanels[y][x].remove(gridPanels[y][x].getComponentCount()-1);
			}
			gridPanels[y][x].setBackground(null);
			gridPanels[y][x].add(new JLabel(new ImageIcon(hazardImage)));
		}
		Point simPos = sm.getSimPosition();
		
		
		x = (int)simPos.getX();
		y = (int)simPos.getY();
		// sim 그리기
		// 이전에 gridPanel에 add되어 있는 것을 지워 sim이 보이게 함
		while(gridPanels[y][x].getComponentCount()>0)
		{
			gridPanels[y][x].remove(gridPanels[y][x].getComponentCount()-1);
		}
		gridPanels[y][x].setBackground(null);
		gridPanels[y][x].add(simLabel);
		
		setVisible(true);
	}
	
	/*
	 *  moveNext버튼을 누르면 실행되는 함수로서 도착여부도 반환받아 메시지를 보여준다.
	 */
	public void moveNext()
	{
		try
		{
			if(addon.moveNext()==false)	// false이면 모든 도착지에 도착
			{
				JOptionPane.showMessageDialog(null, "이미 도착했습니다.");	
			}
		}
		catch(IndexOutOfBoundsException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		display();
	}

	public static void main(String[] args)
	{
		new MainForm();
	}
	
}



