package P_External;

import java.awt.Point;

@SuppressWarnings("serial")
class MapPoint extends Point
{
	private boolean hazard;
	private boolean colorBlob;
	
	/* Constructor
	 * argument:
	 *		(x,y) MapPoint의 좌표
	 */
	MapPoint(int x, int y)
	{
		super(x, y);
		// hazard와 colorBlob은 모두 false로 설정한다
		this.hazard = false;
		this.colorBlob = false;
	}
	
	/* 
	 * @see P_External.MapCoordinate#isHazard()
	 * return:
	 * 		hazard가 존재한다면 true를 리턴
	 * 		존재하지 않는다면 false를 리턴
	 */
	boolean isHazard() 
	{
		return this.hazard;
	}

	/*
	 * @see P_External.MapCoordinate#isColorBlob()
	 * return:
	 * 		colorBlob이 존재한다면 true를 리턴
	 * 		존재하지 않는다면 false를 리턴
	 */
	boolean isColorBlob()
	{
		return this.colorBlob;
	}

	/*
	 * @see P_External.MapCoordinate#setHazrd()
	 * 이 좌표에 hazard를 등록한다
	 */
	void setHazard() 
	{
		this.hazard = true;
	}
	
	/*
	 * @see P_External.MapCoordinate#setColorBlob()
	 * 이 좌표에 colorBlob을 등록한다
	 */
	void setColorBlob() 
	{
		this.colorBlob = true;
	}
	
}
