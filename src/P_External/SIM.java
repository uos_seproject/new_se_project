package P_External;

import java.awt.Point;
import java.util.Random;

public class SIM
{
	private MapManager allMap;
	private Point curCoord;
	private Direction direction;
	Random rand = new Random();
	
	/* Constructor
	 * argument:
	 * 		initPos: SIM의 시작 지점
	 * 		allMap: SIM이 hazard, colorBlob을 탐지 하기 위한 map
	 */
	public SIM(Point initPos, MapManager allMap)
	{
		this.allMap = allMap;
		this.curCoord = new Point(initPos);
		direction = Direction.NORTH;
	}
	
	/*
	 * @see P_External.ISIM#getHazard()
	 * SIM이 보고있는 방향(Direction)에 hazard가 존재하는지 탐지한다
	 * return:
	 * 		SIM이 보고있는 방향에 hazard가 있다면 좌표 형태로 리턴
	 * 		SIM이 보고있는 방향에 hazard가 없다면 null을 리턴
	 */
	public Point getHazard() {
		int curX = (int)curCoord.getX();
		int curY = (int)curCoord.getY();
		int newX = -1, newY = -1;

		// SIM이 보고 있는 방향(북,동,남,서)에 대해서 좌표를 얻는다
		if(direction.equals(Direction.NORTH))
		{
			newX = curX;
			newY = curY-1;
		}
		else if(direction.equals(Direction.EAST))
		{
			newX = curX+1;
			newY = curY;
		}
		else if(direction.equals(Direction.SOUTH))
		{
			newX = curX;
			newY = curY+1;
		}
		else if(direction.equals(Direction.WEST))
		{
			newX = curX-1;
			newY = curY;
		}
		
		// SIM이 보고있는 방향의 좌표가 Map 내 이면서 hazard가 존재한다면 좌표(Point)를 리턴한다
		if(allMap.isInMap(newX, newY) && allMap.getHazard(newX, newY))
		{
			return new Point(newX,newY);
		}
		
		// SIM이 보고있는 방향의 좌표가 Map 밖이거나, hazard가 존재하지 않는다면 null을 리턴한다
		return null;
	}
	
	/*
	 * @see P_External.ISIM#getColorBlobs()
	 * return:
	 * 		colorBlobs의 배열형, 배열길이는 4
	 */
	public Point[] getColorBlobs() 
	{
		Point[] colorBlobs = new Point[4];
		int colorBlobsIndex = 0;
		
		int curX = (int)curCoord.getX();
		int curY = (int)curCoord.getY();
		int newV;
		
		// SIM의 기준으로 모든 방향에 대해서 colorBlob이 존재한다면 좌표를 추가한다
		newV = curY-1;
		if(allMap.getColorBlob(curX, newV))
		{
			colorBlobs[colorBlobsIndex++] = new Point(curX, newV);
		}
		newV = curX+1;
		if(allMap.getColorBlob(newV, curY))
		{
			colorBlobs[colorBlobsIndex++] = new Point(newV, curY);
		}
		newV = curY+1;
		if(allMap.getColorBlob(curX, newV))
		{
			colorBlobs[colorBlobsIndex++] = new Point(curX, newV);
		}
		newV = curX-1;
		if(allMap.getColorBlob(newV, curY))
		{
			colorBlobs[colorBlobsIndex++] = new Point(newV, curY);
		}
		
		// 리턴할 배열의 길이가 4이므로 추가한 좌표가 4개 미만일 경우 남은 자리에 null을 넣어주고 리턴한다
		while(colorBlobsIndex<4)
		{
			colorBlobs[colorBlobsIndex++] = null;
		}
		return colorBlobs;
	}
	
	/*
	 * @see P_External.ISIM#goStraightForward()
	 * SIM에게 앞으로 가라는 명령을 내린다
	 */
	public void goStraightForward()
	{
		int distance;
		int curX = (int)curCoord.getX();
		int curY = (int)curCoord.getY();
		
		if(rand.nextInt(33) == 0)
		{ // 1/33 확률로 Incorrect move를 동작한다
			if(rand.nextBoolean()==true)
			{ // Incorrect move가 동작되었을때 1/2 확률로 2칸 앞으로 가기 위한 거리를 설정한다
				distance = 2;
			}
			else
			{// Incorrect move가 동작되었을 때 1/2 확률로 제자리에 있는다.
			 // 이 경우는 앞으로 갈 필요가 없으므로 바로 리턴한다
				return;
			}
		}
		else 
		{ // Correct move
			distance = 1;
		}
		
		// SIM의 방향에 대해서 distance 만큼 이동한 새 좌표 (newX, newY)를 설정한다
		int newX = curX;
		int newY = curY;
		if(direction.equals(Direction.NORTH))
		{
			newY = (int)curCoord.getY()-distance;
		}
		else if(direction.equals(Direction.EAST))
		{
			newX = (int)curCoord.getX()+distance;
		}
		else if(direction.equals(Direction.SOUTH))
		{
			newY = (int)curCoord.getY()+distance;
		}
		else if(direction.equals(Direction.WEST))
		{
			newX = (int)curCoord.getX()-distance;
		}
		
		// 새 좌표가 맵 내에 있으며, 이 좌표가 hazard가 없을 경우 움직인다
		// hazard가 있을 경우엔 움직이지 않는다.(Incorrect move)
		if(allMap.isInMap(newX, newY) == true &&
		   allMap.getHazard(newX, newY) == false)
		{
			curCoord.move(newX, newY);
		}
	}

	/*
	 * @see P_External.ISIM#turnRight()
	 * SIM의 방향을 시계방향으로 1회 회전한다
	 */
	public void turnRight()
	{
		if(direction.equals(Direction.NORTH)) direction = Direction.EAST;
		else if(direction.equals(Direction.EAST)) direction = Direction.SOUTH;
		else if(direction.equals(Direction.SOUTH)) direction = Direction.WEST;
		else if(direction.equals(Direction.WEST)) direction = Direction.NORTH;
	}
	
	/*
	 * @see P_External.ISIM#getPosition()
	 * return:
	 * 		현재 SIM의 위치
	 */
	public Point getPosition()
	{
		return this.curCoord;
	}
	
	/*
	 * @see P_External.ISIM#getDirection()
	 * return:
	 * 		현재 SIM의 방향
	 */
	public Direction getDirection()
	{
		return this.direction;
	}
}
