package P_External;

/*
 * @see P_External.Direction#isInMap(int, int)
 * 새로 쓴 소프트웨어 공학(정익사, 최은만 저)의 473p
 * 		열거형을 안전하게 비교하기 위한 클래스 캡슐화를 참조하여 구현
 */
public class Direction {
	private static int count = 0;
	public static final Direction NORTH = new Direction(count++);
	public static final Direction EAST = new Direction(count++);
	public static final Direction SOUTH = new Direction(count++);
	public static final Direction WEST = new Direction(count++);
	private int value;
	
	private Direction(int value)
	{
		this.value= value;
	}
	
	public int getValue()
	{
		return this.value;
	}
	
	public boolean equals(Direction arg)
	{
		return this.value == arg.getValue();
	}
	
	@Override
	public String toString()
	{
		if(equals(Direction.NORTH)) return "Direction.NORTH";
		if(equals(Direction.EAST)) return "Direction.EAST";
		if(equals(Direction.SOUTH)) return "Direction.SOUTH";
		if(equals(Direction.WEST)) return "Direction.WEST";
		return "";
	}
	
}
