package P_External;

import java.awt.Point;
import java.util.ArrayList;

public class MapManager
{
	private MapPoint[][] MapData;
	
	//colorBLob와 hazard의 호출은 빈번하므로 MapData와 별개로 ArrayList로 관리한다
	private ArrayList<Point> hazards;
	private ArrayList<Point> colorBlobs;
	
	/* Constructor
	 * argument:
	 * 		hazards: hazard의 배열
	 * 		mapSize: mapSize.X와 mapSize.Y에 크기가 저장되어 있는 Point형
	 */
	public MapManager(Point[] hazards, Point mapSize) throws Exception
	{
		int mapSizeX = (int)mapSize.getX();
		int mapSizeY = (int)mapSize.getY();
		int i,j;
		
		// mapSizeX와 mapSizeY가 1보다 작으면 맵을 생성할 수 없으므로 Exception을 던진다
		if(mapSizeX<1) throw new Exception("mapSize X < 1");
		if(mapSizeY<1) throw new Exception("mapSize Y < 1");

		// this.hazards를 ArrayList<Point>로 초기화 하며, arg로 받은 hazard를 모두 설정한다.
		this.hazards = new ArrayList<Point>();
		for(i=0; i<hazards.length; i++)
		{
			this.hazards.add(hazards[i]);
		}
		
		// this.colorBlob을 ArrayList<Point>로 초기화한다
		this.colorBlobs = new ArrayList<Point>();
		
		// this.MapData를 초기화 하며 2중 loop로 MapPoint를 선언한다
		this.MapData = new MapPoint[mapSizeY][mapSizeX];
		for(i=0; i<mapSizeY; i++)
		{
			for(j=0; j<mapSizeX; j++)
			{
				this.MapData[i][j] = new MapPoint(j, i);
			}
		}
		
		// mapData에 hazard를 등록한다
		int hazardSize = hazards.length;
		for(i=0; i<hazardSize; i++)
		{
			if(this.isInMap((int)hazards[i].getX(), (int)hazards[i].getY()))
			{
				this.MapData[(int)hazards[i].getY()][(int)hazards[i].getX()].setHazard();
			}
			else // hazard의 좌표가 맵 밖에 있다면 Exception을 던진다
			{
				throw new IndexOutOfBoundsException("a hazard is out of the map");
			}
		}
	}
	
	/*
	 * @see P_External.IMapManager#getHazard(int, int)
	 * arg로 들어온 좌표에 hazard의 존재 여부를 리턴한다
	 * argument:
	 *		(x,y): 좌표
	 * return:
	 * 		true: arg의 좌표가 Map 내에 있으며 hazard가 존재
	 * 		false: arg의 좌표가 Map 밖에 있거나, Map 내에 있으면서 hazard가 존재하지 않음.
	 */
	public boolean getHazard(int x, int y)
	{
		if(!isInMapX(x) || !isInMapY(y)) return false;
		return this.MapData[y][x].isHazard();
	}

	/*
	 * @see P_External.IMapManager#getColorBlob(int, int)
	 * arg의 좌표에서 colorBlob의 존재 여부를 리턴
	 * argument:
	 * 		(x,y): 좌표
	 * return:
	 * 		true: arg의 좌표가 Map 내에 있으며 colorBlob이 존재
	 * 		false: arg의 좌표가 Map 밖에 있거나, Map 내에 있으면서 colorBlob이 존재하지않음
	 */
	public boolean getColorBlob(int x, int y)
	{
		if(!isInMapX(x) || !isInMapY(y)) return false;
		return this.MapData[y][x].isColorBlob();
	}

	/*
	 * @see P_External.IMapManager#setHazard(int, int)
	 * (x,y)에 hazard를 등록, this.hazard에도 추가 
	 * argument:
	 * 		(x,y): 좌표
	 */
	public void setHazard(int x, int y) throws IndexOutOfBoundsException
	{
		// (x,y)가 맵 밖에있다면 Exception을 던짐
		if(!isInMapX(x)) throw new IndexOutOfBoundsException("x ("+x+") is out of Map");
		if(!isInMapX(y)) throw new IndexOutOfBoundsException("y ("+y+") is out of Map");

		//MapData와 this.hazards에 hazard를 등록
		this.MapData[y][x].setHazard();
		this.hazards.add(this.MapData[y][x]);
	}

	/*
	 * @see P_External.IMapManager#setHazard(int, int)
	 * (x,y)에 colorBlob을 등록
	 * argument:
	 * 		(x,y) 좌표
	 */
	public void setColorBlob(int x, int y) throws IndexOutOfBoundsException
	{
		// (x,y)가 맵 밖에있다면 Exception을 던짐
		if(!isInMapX(x)) throw new IndexOutOfBoundsException("x ("+x+") is out of Map");
		if(!isInMapX(y)) throw new IndexOutOfBoundsException("y ("+y+") is out of Map");
		
		//MapData와 this.colorBlobs에 colorBlob을 등록
		this.MapData[y][x].setColorBlob();
		this.colorBlobs.add(this.MapData[y][x]);
	}

	/*
	 * @see P_External.IMapManager#getAllHazards()
	 * return:
	 * 		this.hazards의 배열형,
	 * 			this.hazards는 ArrayList이므로 배열형으로 주소를 복사를 하여 리턴
	 */
	public Point[] getAllHazards()
	{
		int size = this.hazards.size();
		Point[] hazardArray = new Point[size];
		for(int i=0; i<size; i++)
		{
			hazardArray[i] = this.hazards.get(i);
		}
		return hazardArray;
	}

	/*
	 * @see P_External.IMapManager#getAllHazards()
	 * return:
	 * 		this.colorBlobs의 배열형,
	 * 			this.colroBlobs는 ArrayList이므로 배열형으로 주소를 복사하여 리턴
	 */
	public Point[] getAllColorBlobs()
	{
		int size = this.colorBlobs.size();
		Point[] colorArray = new Point[size];
		for(int i=0; i<size; i++)
		{
			colorArray[i] = this.colorBlobs.get(i);
		}
		return colorArray;
	}
	
	/*
	 * @see P_External.IMapManager#isInMapX(int)
	 * arg x가 map 내에 있는지 확인
	 * argument:
	 * 		x: 좌표 x
	 * return:
	 * 		true: x가 맵 내에 있음
	 * 		false: x가 맵 밖에 있음
	 */
	public boolean isInMapX(int x)
	{
		if(0<=x && x<this.MapData[0].length) return true;
		return false;
	}
	
	/*
	 * @see P_External.IMapManager#isInMapY(int)
	 * arg y가 map 내에 있는지 확인
	 * argument:
	 * 		y: 좌표 y
	 * return:
	 * 		true: y가 맵 내에 있음
	 * 		false: y가 맵 밖에 있음
	 */
	public boolean isInMapY(int y)
	{
		if(0<=y && y<this.MapData.length) return true;
		return false;
	}
	
	/*
	 * @see P_External.IMapManager#isInMap(int, int)
	 * (x,y)가 맵 내에 있는지 확인
	 * argument:
	 * 		(x,y): 좌표
	 * return:
	 * 		true: (x,y)가 맵 내에 있음
	 * 		false: (x,y)가 맵 밖에 있음
	 */
	public boolean isInMap(int x, int y)
	{
		return isInMapX(x) && isInMapY(y);
	}
}
