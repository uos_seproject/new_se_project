package P_ADD_ON;


import java.awt.Point;
import java.util.Random;

import P_External.Direction;
import P_External.MapManager;
import P_External.SIM;

public class SIMManager
{
	private MapManager allMap;		// 전체 Map
	private Point curCoord;			// 현재 sim의 상태
	private SIM sim;				// sim
	
	public SIMManager(Point[] hazards, Point[] dests, Point simPos, Point mapSize) throws Exception
	{
		this.allMap = new MapManager(hazards, mapSize);
		this.sim = new SIM(simPos,allMap);
		this.curCoord = simPos;
		generateRandThings(simPos, (int)mapSize.getX(), (int)mapSize.getY(), dests);
	}
	
	/*
	 * random하게 hidden hazard/color blob 생성하는 함수
	 */
	private void generateRandThings(Point simPos, int mapSizeX, int mapSizeY, Point[] dests)
	{
		Random rand = new Random();
		int n = (int) (mapSizeX * mapSizeY * 0.4);
			// # of hidden hazards( # of hidden colorBlobs)
		int i, j, randX, randY;
		boolean isOverlappedDest;
		int simX = (int)simPos.getX();
		int simY = (int)simPos.getY();
		for(i=0 ;i<n; i++)
		{// random 좌표를 구함
			do
			{
				randX = rand.nextInt(mapSizeX-1);
				randY = rand.nextInt(mapSizeY-1);
				isOverlappedDest = false;
				for(j=0; j<dests.length; j++)
				{
					if((int)dests[j].getX() == randX && (int)dests[j].getY() == randY)
					{
						isOverlappedDest = true;
						break;
					}
				}
			}while((simX == randX && simY == randY) == true || isOverlappedDest == true);	// sim의 초기위치와 겹쳐 생성 되면 안됨
			
			// 위에서 구한 random좌표를 바탕으로 hazard와 color blob을 분배
			if(rand.nextBoolean() == true)
			{
				this.allMap.setHazard(randX, randY);
			}
			else
			{
				this.allMap.setColorBlob(randX, randY);
			}
		}
	}
	
	/*
	 * Hidden colorBlobs의 정보를 UI에게 전달하는 함수
	 */
	public Point[] getAllColorBlobs()
	{
		return this.allMap.getAllColorBlobs();
	}

	/*
	 * Hidden hazard의 정보를 UI에게 전달하는 함수
	 */
	public Point[] getAllHazards()
	{
		return this.allMap.getAllHazards();
	}

	/*
	 * 현재 sim의 위치 반환하여 UI에 전달하는 함수
	 */
	public Point getSimPosition()
	{
		return this.sim.getPosition();
	}

	/*
	 * 현재 SIM의 방향 반환하는 함수
	 */
	public Direction getSimDirection()
	{
		return this.sim.getDirection();
	}

	/*
	 *  예상한 위치에 도착을 했는지 체크하는 함수
	 */
	boolean isCorrectMove()
	{
		//sim의 현재좌표와 pos(예상된 좌표)를 비교
//		return this.sim.getPosition().equals(this.curCoord);
		Point simPos = sim.getPosition();
		return (int)simPos.getX() == (int)this.curCoord.getX() 
				&& (int)simPos.getY() == (int)this.curCoord.getY();
	}
	
	/*
	 * sim이 90도 회전하는 함수
	 */
	private void turnRight(Point nextCoord)
	{
		Point simPos = sim.getPosition();
		int simX = (int)simPos.getX();
		int simY = (int)simPos.getY();
		int nextX = (int)nextCoord.getX();
		int nextY = (int)nextCoord.getY();
		Direction simD = null;

		// 현재 sim의 위치와 nextCoord의 위치를 비교하여 방향 구하기
		if(nextX - simX>0)
		{
			simD = Direction.EAST;
		}
		else if(nextX-simX<0)
		{
			simD = Direction.WEST;
		}
		else if(nextY-simY<0)
		{
			simD = Direction.NORTH;
		}
		else if(nextY-simY>0)
		{
			simD = Direction.SOUTH;
		}
		
		if(simD == null)
		{
			throw new NullPointerException("simD is Null.");
		}
		// 방향에 맞을 때까지 오른쪽으로 회전
		while(!this.sim.getDirection().equals(simD))
		{
			this.sim.turnRight();
		}
	}
	
	/*
	 * SIM이 가야할 좌표에 맞게 방향 회전후 움직이며, colorBlob과 Hazard를 센싱하여 존재하면 갱신하는 함수
	 */
	boolean moveNext(Point nextCoord, MapManager addonMap)
	{
		Point[] colorBlobs;
		Point hazard;
		
		this.turnRight(nextCoord);
		
		colorBlobs = this.getColorBlobs();
		hazard = this.getHazard();
		
		// colorBlob가 있으면 addonMap에 갱신
		if(colorBlobs != null)
		{
			for(int i=0; i<colorBlobs.length && colorBlobs[i] != null; i++)
			{
				addonMap.setColorBlob((int)colorBlobs[i].getX(), (int)colorBlobs[i].getY());
			}
		}
		
		// hazard가 있으면 addonMap에 갱신
		if(hazard != null)
		{
			addonMap.setHazard((int)hazard.getX(), (int)hazard.getY());
			return false;
		}
		
		this.sim.goStraightForward();	// sim을 다음 위치로 진행
		this.curCoord.move((int)nextCoord.getX(), (int)nextCoord.getY());
		
		return true;
	}
	
	/*
	 * sim Hazard센서로 Hazard 좌표를 얻어와 반환하는 함수
	 */
	private Point getHazard()
	{
		return this.sim.getHazard();
	}
	
	/*
	 * sim colorBlob센서로 colorBlobs의 좌표들을 얻어와 반환하는 함수
	 */
	private Point[] getColorBlobs(){
		return this.sim.getColorBlobs();
	}

}
