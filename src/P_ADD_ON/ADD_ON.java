package P_ADD_ON;

import java.awt.Point;

import P_External.MapManager;
import P_Path.Node;
import P_Path.PathManager;

public class ADD_ON 
{
	private Node[] dests;	//destination node array
	private int destIndex;	//current destination index
	private PathManager pm;	//pathmanager pm
	private SIMManager sm;	//simmanager sm
	private MapManager addonMap; //mapmanager addonmap
	
	public ADD_ON(Point[] hazards, Point[] dests, Point simPos, Point mapSize, SIMManager sm) throws Exception
	{
		this.addonMap = new MapManager(hazards, mapSize);	//객체 생성 
		for(int i=0; i<dests.length; i++)
		{//addonMap에 dests 정보가 'Out of Bounds' 인지 체크 
			if(this.addonMap.isInMap((int)dests[i].getX(), (int)dests[i].getY())==false)
			{
				throw new IndexOutOfBoundsException("dest is out of map");
			}
		}
		
		destSort(dests, simPos);							//목적지(predefined spot) 가까운 순으로 정렬 
		
		this.sm = sm;
		this.pm = new PathManager(this.addonMap);
		this.pm.setStart(simPos);							//시작점 설정 
		this.pm.setDest(this.dests[0]);						//첫번째 목적지 설정 
		this.pm.generatePath();								//첫번째 목적지에 대한 path 설정 
		this.destIndex=0;									//첫번째 목적지의 index부여 
	}
	
	/*
	 * 목적지를 sim 가까운 순서로 sorting한다. 
	 */
	private void destSort(Point[] list, Point simPos)
	{
		this.dests = new Node[list.length];

		for(int i=0;i<list.length;i++)
		{
			this.dests[i] = new Node(list[i], simPos, null);
		}
		
		for(int i=0; i<this.dests.length; i++)
		{
			for(int j=0; j<this.dests.length-i-1;j++)
			{//현재 위치와 목적지 사이의 거리에 따른 '버블 소트' 
				if(this.dests[j].getCost() > this.dests[j+1].getCost())	//거리 비교 
				{ // 'SWAP' dests[j] and dests[j+1]
					Node temp = this.dests[j];
					this.dests[j] = this.dests[j+1];
					this.dests[j+1] = temp;
				}
			}
		}
	}
			
	/*
	 * colorBlobs의 정보를 UI에게 전달하는 함수
	 */
	public Point[] getDiscoveredColorBlobs()
	{
		return this.addonMap.getAllColorBlobs();
	}

	/*
	 * hazard의 정보를 UI에게 전달하는 함수
	 */
	public Point[] getDiscoverdHazards()
	{
		return this.addonMap.getAllHazards();
	}
	
	/*
	 * 다음 좌표로 이동 
	 */
	public boolean moveNext()
	{
		if(this.destIndex == this.dests.length)
		{//목적지에 도착했을 경우
			return false;
		}
		
		Point nextPos = pm.getNextPath();						//다음에 이동해야 할 좌표를 얻는다
		if(sm.moveNext(nextPos, addonMap)) 
		{//하자드가 없는 경우
			if(sm.isCorrectMove() == false)
			{//if sim is 'not right position'
				pm.setStart(sm.getSimPosition());					//현재 좌표 재설정 
				pm.generatePath();									//현재 좌표에 대한 path 재설정  
			}	
			//if sim is 'right position'
			if(isArrived()==true)
			{//if sim이 현재 목적지에 도착한 경우 
				pm.setStart(dests[destIndex]);					//현재 목적지를 출발점으로 설정  
				destIndex++;									//목적지 인덱스를 다음 목적지 인덱스로 설정 
				if(destIndex == dests.length) return true;		//모든 목적지를 모두 간 경우 
				pm.setDest(dests[destIndex]);					//다음 목적지를 목적지로 설정 (모든 목적지를 모두 가지 않은 경우) 
				pm.generatePath();								//경로 설정 
			}
		}
		else 
		{//하자드가 있는 경우
			pm.setStart(sm.getSimPosition());						//현재 위치를 시작점으로 부여 
			pm.generatePath();										//경로 재설정 
			this.moveNext();										//다시 moveNext 재호출 
		}
		return true;
	}
	
	/*
	 * 도착 여부를 확인하는 함수
	 */
	private boolean isArrived()
	{
		Point simPos = sm.getSimPosition();
		if((int)this.dests[destIndex].getX() == (int)simPos.getX() 
				&& (int)this.dests[destIndex].getY() == (int)simPos.getY())
		{//현재 목적지에 sim이 도착한 경우,  
			return true;
		}
		else
		{//그렇지 않은 경우, 
			return false;
		}
	}
}
